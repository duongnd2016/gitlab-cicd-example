package com.ocean.myblog.web.rest;

import com.ocean.myblog.MyblogApp;
import com.ocean.myblog.domain.Author;
import com.ocean.myblog.domain.Post;
import com.ocean.myblog.repository.AuthorRepository;
import com.ocean.myblog.service.AuthorService;
import com.ocean.myblog.service.dto.AuthorDTO;
import com.ocean.myblog.service.mapper.AuthorMapper;
import com.ocean.myblog.service.dto.AuthorCriteria;
import com.ocean.myblog.service.AuthorQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AuthorResource} REST controller.
 */
@SpringBootTest(classes = MyblogApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AuthorResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_ADDED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ADDED = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ADDED = LocalDate.ofEpochDay(-1L);

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AuthorQueryService authorQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAuthorMockMvc;

    private Author author;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createEntity(EntityManager em) {
        Author author = new Author()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .birthdate(DEFAULT_BIRTHDATE)
            .added(DEFAULT_ADDED);
        return author;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createUpdatedEntity(EntityManager em) {
        Author author = new Author()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .birthdate(UPDATED_BIRTHDATE)
            .added(UPDATED_ADDED);
        return author;
    }

    @BeforeEach
    public void initTest() {
        author = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthor() throws Exception {
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the Author
        AuthorDTO authorDTO = authorMapper.toDto(author);
        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isCreated());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate + 1);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testAuthor.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testAuthor.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAuthor.getBirthdate()).isEqualTo(DEFAULT_BIRTHDATE);
        assertThat(testAuthor.getAdded()).isEqualTo(DEFAULT_ADDED);
    }

    @Test
    @Transactional
    public void createAuthorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the Author with an existing ID
        author.setId(1L);
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setFirstName(null);

        // Create the Author, which fails.
        AuthorDTO authorDTO = authorMapper.toDto(author);

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setLastName(null);

        // Create the Author, which fails.
        AuthorDTO authorDTO = authorMapper.toDto(author);

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setEmail(null);

        // Create the Author, which fails.
        AuthorDTO authorDTO = authorMapper.toDto(author);

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBirthdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setBirthdate(null);

        // Create the Author, which fails.
        AuthorDTO authorDTO = authorMapper.toDto(author);

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddedIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorRepository.findAll().size();
        // set the field null
        author.setAdded(null);

        // Create the Author, which fails.
        AuthorDTO authorDTO = authorMapper.toDto(author);

        restAuthorMockMvc.perform(post("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAuthors() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].birthdate").value(hasItem(DEFAULT_BIRTHDATE.toString())))
            .andExpect(jsonPath("$.[*].added").value(hasItem(DEFAULT_ADDED.toString())));
    }
    
    @Test
    @Transactional
    public void getAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get the author
        restAuthorMockMvc.perform(get("/api/authors/{id}", author.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(author.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.birthdate").value(DEFAULT_BIRTHDATE.toString()))
            .andExpect(jsonPath("$.added").value(DEFAULT_ADDED.toString()));
    }


    @Test
    @Transactional
    public void getAuthorsByIdFiltering() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        Long id = author.getId();

        defaultAuthorShouldBeFound("id.equals=" + id);
        defaultAuthorShouldNotBeFound("id.notEquals=" + id);

        defaultAuthorShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAuthorShouldNotBeFound("id.greaterThan=" + id);

        defaultAuthorShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAuthorShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAuthorsByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName equals to DEFAULT_FIRST_NAME
        defaultAuthorShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the authorList where firstName equals to UPDATED_FIRST_NAME
        defaultAuthorShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByFirstNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName not equals to DEFAULT_FIRST_NAME
        defaultAuthorShouldNotBeFound("firstName.notEquals=" + DEFAULT_FIRST_NAME);

        // Get all the authorList where firstName not equals to UPDATED_FIRST_NAME
        defaultAuthorShouldBeFound("firstName.notEquals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultAuthorShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the authorList where firstName equals to UPDATED_FIRST_NAME
        defaultAuthorShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName is not null
        defaultAuthorShouldBeFound("firstName.specified=true");

        // Get all the authorList where firstName is null
        defaultAuthorShouldNotBeFound("firstName.specified=false");
    }
                @Test
    @Transactional
    public void getAllAuthorsByFirstNameContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName contains DEFAULT_FIRST_NAME
        defaultAuthorShouldBeFound("firstName.contains=" + DEFAULT_FIRST_NAME);

        // Get all the authorList where firstName contains UPDATED_FIRST_NAME
        defaultAuthorShouldNotBeFound("firstName.contains=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByFirstNameNotContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where firstName does not contain DEFAULT_FIRST_NAME
        defaultAuthorShouldNotBeFound("firstName.doesNotContain=" + DEFAULT_FIRST_NAME);

        // Get all the authorList where firstName does not contain UPDATED_FIRST_NAME
        defaultAuthorShouldBeFound("firstName.doesNotContain=" + UPDATED_FIRST_NAME);
    }


    @Test
    @Transactional
    public void getAllAuthorsByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName equals to DEFAULT_LAST_NAME
        defaultAuthorShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the authorList where lastName equals to UPDATED_LAST_NAME
        defaultAuthorShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByLastNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName not equals to DEFAULT_LAST_NAME
        defaultAuthorShouldNotBeFound("lastName.notEquals=" + DEFAULT_LAST_NAME);

        // Get all the authorList where lastName not equals to UPDATED_LAST_NAME
        defaultAuthorShouldBeFound("lastName.notEquals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultAuthorShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the authorList where lastName equals to UPDATED_LAST_NAME
        defaultAuthorShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName is not null
        defaultAuthorShouldBeFound("lastName.specified=true");

        // Get all the authorList where lastName is null
        defaultAuthorShouldNotBeFound("lastName.specified=false");
    }
                @Test
    @Transactional
    public void getAllAuthorsByLastNameContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName contains DEFAULT_LAST_NAME
        defaultAuthorShouldBeFound("lastName.contains=" + DEFAULT_LAST_NAME);

        // Get all the authorList where lastName contains UPDATED_LAST_NAME
        defaultAuthorShouldNotBeFound("lastName.contains=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllAuthorsByLastNameNotContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where lastName does not contain DEFAULT_LAST_NAME
        defaultAuthorShouldNotBeFound("lastName.doesNotContain=" + DEFAULT_LAST_NAME);

        // Get all the authorList where lastName does not contain UPDATED_LAST_NAME
        defaultAuthorShouldBeFound("lastName.doesNotContain=" + UPDATED_LAST_NAME);
    }


    @Test
    @Transactional
    public void getAllAuthorsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email equals to DEFAULT_EMAIL
        defaultAuthorShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the authorList where email equals to UPDATED_EMAIL
        defaultAuthorShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email not equals to DEFAULT_EMAIL
        defaultAuthorShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the authorList where email not equals to UPDATED_EMAIL
        defaultAuthorShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultAuthorShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the authorList where email equals to UPDATED_EMAIL
        defaultAuthorShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email is not null
        defaultAuthorShouldBeFound("email.specified=true");

        // Get all the authorList where email is null
        defaultAuthorShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllAuthorsByEmailContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email contains DEFAULT_EMAIL
        defaultAuthorShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the authorList where email contains UPDATED_EMAIL
        defaultAuthorShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAuthorsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where email does not contain DEFAULT_EMAIL
        defaultAuthorShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the authorList where email does not contain UPDATED_EMAIL
        defaultAuthorShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate equals to DEFAULT_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.equals=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate equals to UPDATED_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.equals=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate not equals to DEFAULT_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.notEquals=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate not equals to UPDATED_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.notEquals=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate in DEFAULT_BIRTHDATE or UPDATED_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.in=" + DEFAULT_BIRTHDATE + "," + UPDATED_BIRTHDATE);

        // Get all the authorList where birthdate equals to UPDATED_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.in=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate is not null
        defaultAuthorShouldBeFound("birthdate.specified=true");

        // Get all the authorList where birthdate is null
        defaultAuthorShouldNotBeFound("birthdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate is greater than or equal to DEFAULT_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.greaterThanOrEqual=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate is greater than or equal to UPDATED_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.greaterThanOrEqual=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate is less than or equal to DEFAULT_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.lessThanOrEqual=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate is less than or equal to SMALLER_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.lessThanOrEqual=" + SMALLER_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsLessThanSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate is less than DEFAULT_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.lessThan=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate is less than UPDATED_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.lessThan=" + UPDATED_BIRTHDATE);
    }

    @Test
    @Transactional
    public void getAllAuthorsByBirthdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where birthdate is greater than DEFAULT_BIRTHDATE
        defaultAuthorShouldNotBeFound("birthdate.greaterThan=" + DEFAULT_BIRTHDATE);

        // Get all the authorList where birthdate is greater than SMALLER_BIRTHDATE
        defaultAuthorShouldBeFound("birthdate.greaterThan=" + SMALLER_BIRTHDATE);
    }


    @Test
    @Transactional
    public void getAllAuthorsByAddedIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added equals to DEFAULT_ADDED
        defaultAuthorShouldBeFound("added.equals=" + DEFAULT_ADDED);

        // Get all the authorList where added equals to UPDATED_ADDED
        defaultAuthorShouldNotBeFound("added.equals=" + UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added not equals to DEFAULT_ADDED
        defaultAuthorShouldNotBeFound("added.notEquals=" + DEFAULT_ADDED);

        // Get all the authorList where added not equals to UPDATED_ADDED
        defaultAuthorShouldBeFound("added.notEquals=" + UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsInShouldWork() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added in DEFAULT_ADDED or UPDATED_ADDED
        defaultAuthorShouldBeFound("added.in=" + DEFAULT_ADDED + "," + UPDATED_ADDED);

        // Get all the authorList where added equals to UPDATED_ADDED
        defaultAuthorShouldNotBeFound("added.in=" + UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added is not null
        defaultAuthorShouldBeFound("added.specified=true");

        // Get all the authorList where added is null
        defaultAuthorShouldNotBeFound("added.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added is greater than or equal to DEFAULT_ADDED
        defaultAuthorShouldBeFound("added.greaterThanOrEqual=" + DEFAULT_ADDED);

        // Get all the authorList where added is greater than or equal to UPDATED_ADDED
        defaultAuthorShouldNotBeFound("added.greaterThanOrEqual=" + UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added is less than or equal to DEFAULT_ADDED
        defaultAuthorShouldBeFound("added.lessThanOrEqual=" + DEFAULT_ADDED);

        // Get all the authorList where added is less than or equal to SMALLER_ADDED
        defaultAuthorShouldNotBeFound("added.lessThanOrEqual=" + SMALLER_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsLessThanSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added is less than DEFAULT_ADDED
        defaultAuthorShouldNotBeFound("added.lessThan=" + DEFAULT_ADDED);

        // Get all the authorList where added is less than UPDATED_ADDED
        defaultAuthorShouldBeFound("added.lessThan=" + UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void getAllAuthorsByAddedIsGreaterThanSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get all the authorList where added is greater than DEFAULT_ADDED
        defaultAuthorShouldNotBeFound("added.greaterThan=" + DEFAULT_ADDED);

        // Get all the authorList where added is greater than SMALLER_ADDED
        defaultAuthorShouldBeFound("added.greaterThan=" + SMALLER_ADDED);
    }


    @Test
    @Transactional
    public void getAllAuthorsByPostsIsEqualToSomething() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);
        Post posts = PostResourceIT.createEntity(em);
        em.persist(posts);
        em.flush();
        author.addPosts(posts);
        authorRepository.saveAndFlush(author);
        Long postsId = posts.getId();

        // Get all the authorList where posts equals to postsId
        defaultAuthorShouldBeFound("postsId.equals=" + postsId);

        // Get all the authorList where posts equals to postsId + 1
        defaultAuthorShouldNotBeFound("postsId.equals=" + (postsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAuthorShouldBeFound(String filter) throws Exception {
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].birthdate").value(hasItem(DEFAULT_BIRTHDATE.toString())))
            .andExpect(jsonPath("$.[*].added").value(hasItem(DEFAULT_ADDED.toString())));

        // Check, that the count call also returns 1
        restAuthorMockMvc.perform(get("/api/authors/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAuthorShouldNotBeFound(String filter) throws Exception {
        restAuthorMockMvc.perform(get("/api/authors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAuthorMockMvc.perform(get("/api/authors/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAuthor() throws Exception {
        // Get the author
        restAuthorMockMvc.perform(get("/api/authors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Update the author
        Author updatedAuthor = authorRepository.findById(author.getId()).get();
        // Disconnect from session so that the updates on updatedAuthor are not directly saved in db
        em.detach(updatedAuthor);
        updatedAuthor
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .birthdate(UPDATED_BIRTHDATE)
            .added(UPDATED_ADDED);
        AuthorDTO authorDTO = authorMapper.toDto(updatedAuthor);

        restAuthorMockMvc.perform(put("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isOk());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testAuthor.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testAuthor.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAuthor.getBirthdate()).isEqualTo(UPDATED_BIRTHDATE);
        assertThat(testAuthor.getAdded()).isEqualTo(UPDATED_ADDED);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthor() throws Exception {
        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Create the Author
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAuthorMockMvc.perform(put("/api/authors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        int databaseSizeBeforeDelete = authorRepository.findAll().size();

        // Delete the author
        restAuthorMockMvc.perform(delete("/api/authors/{id}", author.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
