-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: myblog
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'Kaley','Lindgren','troob@example.org','2015-07-29','2004-10-15 10:20:03'),(2,'Kelsie','Predovic','dmante@example.com','1975-05-10','2009-09-30 01:07:01'),(3,'Jared','Crist','zdavis@example.net','1982-02-20','1989-09-10 13:09:40'),(4,'Jovani','Pacocha','leannon.raegan@example.com','1992-01-26','2016-08-08 03:41:55'),(5,'Mikel','Brown','stephanie73@example.org','2005-07-29','1980-03-31 05:44:11'),(6,'Haleigh','Schmitt','whyatt@example.net','1994-06-01','1970-06-29 03:39:38'),(7,'Shana','Pouros','auer.amos@example.com','2005-07-13','2005-11-04 01:21:13'),(8,'Albin','Hettinger','hassan62@example.org','1977-05-25','2004-01-25 13:38:51'),(9,'Coleman','Effertz','vida27@example.org','1976-10-05','2005-11-02 08:22:43'),(10,'Shad','Cummings','sanford61@example.net','2006-06-19','1976-10-30 18:24:17'),(11,'Ardella','Spencer','marley11@example.org','1998-05-02','1977-08-16 17:03:44'),(12,'Lorine','Bashirian','glehner@example.com','1988-01-19','1994-01-05 01:20:09'),(13,'Clifford','Nikolaus','henriette.casper@example.org','1972-04-15','1974-09-29 10:43:21'),(14,'Elva','Hilpert','santino20@example.org','2005-10-28','2017-10-13 15:48:42'),(15,'Ciara','Koch','newton21@example.org','1973-02-06','1998-07-07 02:03:40'),(16,'Elinor','Fahey','westley54@example.net','1973-06-04','1974-08-01 08:04:56'),(17,'Braxton','Pfeffer','vern.morissette@example.net','1999-09-13','2002-02-01 20:35:45'),(18,'Teagan','Armstrong','royce.monahan@example.org','1993-04-08','1976-12-30 21:50:25'),(19,'Tyrel','Altenwerth','xdietrich@example.com','1982-06-09','1980-11-02 19:24:43'),(20,'Arden','Hilll','marguerite.gerlach@example.org','2010-08-02','1980-06-01 13:34:10'),(21,'Alexandro','Legros','zmcclure@example.org','2019-04-29','2001-05-21 20:04:28'),(22,'Susana','Wehner','trantow.otto@example.com','1974-12-22','1986-06-27 07:11:07'),(23,'Estella','Zemlak','dyost@example.org','2016-07-27','1972-02-11 15:04:43'),(24,'Henry','Carter','haylie15@example.net','2009-09-20','1992-11-16 03:15:12'),(25,'Misty','Heidenreich','kpacocha@example.com','2000-07-16','1997-04-21 21:59:00'),(26,'Zelma','Hills','ekilback@example.com','2007-10-28','1974-09-12 10:01:41'),(27,'Alexandro','Weimann','hhettinger@example.org','2017-07-09','1985-02-24 12:36:40'),(28,'Bridie','Nienow','terence.kirlin@example.org','2005-01-03','1991-01-01 22:17:47'),(29,'Taryn','Greenfelder','rosalinda.kshlerin@example.com','1974-04-14','1994-03-12 10:43:09'),(30,'Daphney','Littel','thermann@example.com','2002-11-05','1998-04-24 06:33:53'),(31,'Karlie','Bailey','timothy10@example.com','2002-01-13','1982-10-03 17:23:39'),(32,'Henri','Okuneva','jarvis.stehr@example.org','1989-10-22','1991-07-19 18:38:11'),(33,'Monique','Jenkins','murray.gottlieb@example.org','1979-10-25','2015-10-10 15:40:12'),(34,'Mervin','Schmidt','becker.jovany@example.net','2010-06-07','2010-06-19 20:54:12'),(35,'Mariela','Hackett','birdie.brekke@example.org','1982-10-08','2007-07-01 13:35:23'),(36,'Tyrell','Rippin','isobel75@example.com','2001-07-22','1992-02-24 06:58:21'),(37,'Karolann','Kreiger','greenfelder.alexandrea@example.org','1975-10-15','2004-04-14 16:58:59'),(38,'Izaiah','Leannon','bailey.lamar@example.org','2013-01-19','2013-08-04 17:43:04'),(39,'Holly','Hilll','bogan.jonathan@example.org','2010-06-16','1973-08-13 22:26:08'),(40,'Jordon','Kovacek','zieme.iliana@example.com','1998-05-14','2012-01-30 14:07:26'),(41,'Judson','Carter','estefania.hudson@example.com','2014-10-13','1977-01-04 01:23:41'),(42,'Keira','Miller','kariane47@example.net','1993-07-08','2003-08-22 14:44:30'),(43,'Russell','Hoeger','vullrich@example.net','1982-08-12','1984-08-12 18:40:18'),(44,'Talon','Dietrich','cedrick.ullrich@example.com','2015-08-31','2006-05-06 12:20:43'),(45,'Tyrese','Schuster','kovacek.zachary@example.org','1996-07-20','2013-12-21 14:21:25'),(46,'Liana','Cormier','tshields@example.com','2003-06-22','2007-05-22 03:28:01'),(47,'Justyn','Kris','hcassin@example.net','2008-01-17','2009-07-30 05:08:12'),(48,'Michael','Haag','cmiller@example.com','2008-09-27','1971-03-26 06:37:40'),(49,'Korey','Kiehn','kovacek.damon@example.org','1984-02-29','1983-01-01 20:01:34'),(50,'Giovani','Steuber','laron55@example.org','2005-05-28','1991-05-14 11:35:50'),(51,'Kim','Swaniawski','micaela.vandervort@example.com','2003-10-11','2006-07-21 04:58:04'),(52,'Lizeth','Cormier','eugene51@example.org','2014-01-09','2005-10-18 20:50:31'),(53,'Janick','Trantow','rodriguez.annamarie@example.com','1987-08-15','1997-07-11 07:21:34'),(54,'Elwyn','Carter','janiya35@example.org','1982-11-21','1978-09-20 16:23:12'),(55,'Christ','Moen','lora.dibbert@example.org','1989-06-19','1983-04-08 10:22:22'),(56,'Domingo','Bartell','pkunze@example.net','1978-01-09','1984-01-08 03:53:23'),(57,'Alessia','Schulist','arjun.dach@example.com','1980-09-22','1985-09-29 01:01:54'),(58,'Colleen','Hirthe','adolfo32@example.com','2006-09-28','2006-11-24 02:54:38'),(59,'Sydney','Doyle','marvin.barrett@example.com','1973-10-30','1995-12-07 17:02:51'),(60,'Kathryne','Shanahan','orrin.feeney@example.org','2009-06-16','1981-08-22 11:58:11'),(61,'Sonny','Jenkins','spencer.maureen@example.net','1982-04-29','1997-07-06 04:29:01'),(62,'Alexandria','Ankunding','tyra82@example.org','1973-02-06','1980-04-06 03:49:40'),(63,'Annabelle','Bradtke','fritsch.maegan@example.net','2006-06-24','1983-11-11 05:49:18'),(64,'Mortimer','Weber','qchamplin@example.net','2002-09-04','1970-09-13 02:54:46'),(65,'Ruby','Connelly','nwisozk@example.org','1987-06-06','2005-01-29 05:43:06'),(66,'Dewitt','Jast','lebsack.elvera@example.org','1972-08-27','1973-11-14 14:59:12'),(67,'Kenyon','Altenwerth','nframi@example.net','1987-03-19','2006-08-03 08:23:12'),(68,'Dixie','Ledner','myrtis.lind@example.org','2005-04-15','2005-09-01 07:29:51'),(69,'Soledad','Windler','katarina08@example.net','1970-05-30','1973-05-06 17:00:46'),(70,'Elsie','Schiller','antonina28@example.com','1997-11-10','2011-06-23 01:36:41'),(71,'Aliya','Block','cormier.dorthy@example.net','1983-05-19','1981-08-10 22:49:10'),(72,'Abigayle','Littel','yfadel@example.net','2000-02-07','2005-05-11 07:00:07'),(73,'Cordelia','Carter','medhurst.joey@example.net','2008-05-21','1981-05-24 19:28:20'),(74,'Ada','Jaskolski','jlittel@example.net','1979-03-16','1970-09-09 17:51:10'),(75,'Jamal','Considine','kris.cameron@example.net','2019-06-16','2001-01-16 12:19:55'),(76,'Derek','Effertz','west.mireya@example.net','1990-03-23','1975-04-07 14:17:45'),(77,'Newell','Denesik','ybode@example.com','1973-03-23','1983-09-06 00:32:03'),(78,'Nathanial','Jakubowski','floy46@example.net','1994-05-22','2018-05-19 01:46:10'),(79,'Barton','Marks','ubernier@example.org','1985-09-03','1970-02-11 14:55:31'),(80,'Malinda','Gottlieb','collins.flo@example.net','1996-03-03','1984-05-27 00:54:54'),(81,'Daija','VonRueden','corkery.meaghan@example.net','1990-12-10','1979-11-05 21:43:21'),(82,'Camylle','Heidenreich','jennifer80@example.net','2001-03-31','2007-07-18 09:22:11'),(83,'Rosa','Kihn','mleffler@example.org','1977-04-25','1980-01-12 20:54:17'),(84,'Clemens','Steuber','chester.heathcote@example.org','2005-03-04','1999-09-03 06:47:32'),(85,'Trenton','Olson','wisoky.chyna@example.net','1974-01-11','1999-02-08 22:17:31'),(86,'Dawson','Cremin','moore.dante@example.com','1990-10-29','2009-01-12 20:58:02'),(87,'Jeremie','Blanda','ashlee.carter@example.com','1989-02-06','2002-07-25 05:29:18'),(88,'Stone','Treutel','anicolas@example.net','1977-04-14','2002-02-19 11:13:18'),(89,'Kurt','Lynch','jennifer44@example.org','2002-03-01','1978-05-31 01:10:52'),(90,'Efrain','McKenzie','robert.hoeger@example.org','2004-06-19','2003-09-24 16:02:07'),(91,'Dorris','Schneider','ncummerata@example.net','2008-07-13','1990-10-28 07:22:02'),(92,'Josianne','Doyle','wava44@example.org','1975-06-16','2009-01-12 10:04:50'),(93,'Bud','Lesch','larson.lina@example.org','2011-05-23','1988-01-17 11:34:50'),(94,'Leif','Mills','reilly.batz@example.net','2008-04-27','1995-02-11 06:59:18'),(95,'Camilla','Murray','lkeebler@example.net','2006-12-20','1997-03-02 23:30:12'),(96,'Buddy','Kozey','dvandervort@example.net','2006-07-07','1983-10-22 05:33:06'),(97,'Annetta','Vandervort','akeem83@example.com','2001-10-09','2009-10-11 22:58:41'),(98,'Nayeli','Shanahan','hkshlerin@example.com','2005-05-14','2016-08-15 09:44:07'),(99,'Lelia','Prosacco','skiles.salvador@example.org','2004-03-17','2006-03-04 07:59:55'),(100,'Yasmine','Wintheiser','fglover@example.com','1979-02-09','2010-09-10 00:51:13');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2020-03-21 03:34:19',1,'EXECUTED','8:c5bfc567913b118109a43e981cd02883','createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; addForeignKeyConstraint baseTableName=jhi_user_authority, constraintName=fk_authority_name, ...','',NULL,'3.8.7',NULL,NULL,'4761658537');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (1,'admin','2020-03-21 03:34:27','AUTHENTICATION_SUCCESS'),(2,'admin','2020-03-21 03:35:41','AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(2,'anonymoususer','$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO','Anonymous','User','anonymous@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(4,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','','','en',NULL,NULL,'system',NULL,NULL,'system',NULL);
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(1,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,'Blanditiis tempora non reiciendis sint maiores cum nobis.','Necessitatibus ducimus quisquam nihil impedit quasi. Et assumenda fugit expedita temporibus corporis laboriosam occaecati sit. Quis cumque voluptatem rem cupiditate officiis incidunt.','Culpa rerum sunt sed aut suscipit. Facilis saepe harum iste. Esse iure vitae deserunt recusandae a rerum exercitationem.','2005-09-06'),(2,2,'Debitis cum ipsum quam.','Illum rerum maiores dolore non eum magni quibusdam. Culpa nihil dignissimos ipsum.','Nihil atque dolores ea ea. Veritatis aliquid ea laudantium. Soluta temporibus reprehenderit quia omnis nobis reprehenderit ipsum. Odio exercitationem iste illum.','2017-11-04'),(3,3,'Est et ratione voluptas magnam.','Possimus officia temporibus veritatis sit rerum at similique. Similique a dolores et et deserunt. Inventore qui aut odit quisquam maiores.','Est nobis labore iure ea. Quisquam in deleniti excepturi consequuntur aut magni eum. Sapiente dolore officia esse suscipit aperiam qui.','2014-08-21'),(4,4,'Quia dolores libero facere magni consequatur.','Sunt non in dicta consequatur possimus. Vel iure consequatur rerum. Enim repellat vel voluptate illum. Molestiae ut aperiam quia ipsam animi illo perspiciatis.','Mollitia cumque in cum. Omnis numquam et consequuntur. Nobis reprehenderit temporibus occaecati ipsam.','1997-05-30'),(5,5,'Sed et architecto sit voluptatem dolorem maiores libero.','Facere ut iste dicta numquam. Repellat consequatur vitae neque reiciendis inventore soluta soluta. Sed beatae blanditiis tenetur amet laborum consequatur. Tenetur est veritatis tempore et illum fuga rerum.','Sed sunt ea dolor qui non non laboriosam. Ut dolorem fugiat magnam praesentium fugit dolorum. Veniam corporis recusandae sint explicabo esse eum amet.','1985-09-05'),(6,6,'Voluptatem et rem sed qui quidem.','A asperiores corrupti consequatur. Quidem atque expedita atque occaecati. Aut sapiente aut impedit animi.','Ducimus qui veritatis consequatur id eum eaque alias. Et praesentium aspernatur labore dolorum odit fugit. Doloribus commodi qui iusto rerum. Commodi et et consequatur qui illo.','1987-06-27'),(7,7,'Iusto labore sed sunt voluptatum harum magnam.','Mollitia magni et et dicta sequi consectetur aliquid. Tempore in quidem est nulla quisquam dolor voluptatem. Nesciunt nulla tempore ut. Enim et similique mollitia ut.','Eum ut in accusamus natus. Ducimus enim omnis modi non facilis. Incidunt quo non cumque quibusdam nihil qui soluta.','2012-02-21'),(8,8,'Commodi et id necessitatibus dolores minima quos.','Assumenda sed ullam suscipit et porro quia. Minus omnis id vero mollitia veniam in. Culpa quae amet necessitatibus sed doloremque omnis.','Dolore inventore cum iusto similique eius. Porro adipisci autem et qui et quis ut.','1988-04-20'),(9,9,'In ipsa delectus et est consequuntur.','Officiis velit laboriosam exercitationem consequatur saepe. Quasi ut quae saepe qui eum quo. Quis sit recusandae qui aliquid expedita officiis sed qui. Omnis alias sint nostrum iure quibusdam ipsa occaecati.','Repudiandae dolor similique nemo dolorem iste repellendus. Totam aliquam minus nihil culpa dignissimos.','1994-11-18'),(10,10,'Perspiciatis dolorum nulla praesentium id.','Facilis omnis provident est hic eum dolor. Aperiam sed nihil hic molestiae unde eos.','Voluptas sit ea recusandae doloremque rem autem. Animi et quaerat rem recusandae et. Nemo consequuntur fugit quia quae ad aut earum.','1970-05-01'),(11,11,'Sapiente sed praesentium repudiandae aut aut quidem explicabo placeat.','Eligendi saepe quae molestiae est rerum. Sit ab eveniet non quibusdam alias. Molestias iusto rerum tempora praesentium. Libero error sed laboriosam ut expedita sunt explicabo minus. Labore et nesciunt facere impedit.','Culpa perferendis et natus et. Facilis rem consequatur nesciunt rerum accusantium et. Aliquam quo soluta velit voluptatem recusandae.','1975-04-18'),(12,12,'Quasi consequatur illum occaecati aut qui.','Voluptates quis aperiam est pariatur laudantium tempore. Aut quidem voluptas tenetur rem et error aliquam dolor.','Et consequatur nihil repudiandae amet nihil. Id blanditiis vel qui quam aut doloribus. Repudiandae sequi necessitatibus cum maiores ducimus.','1975-09-11'),(13,13,'Rem incidunt sit labore earum voluptas aperiam consequatur voluptatem.','Error praesentium non quae eos tempora. Accusantium dolore optio vel et voluptatem voluptatem. Quia omnis repellendus vitae ut ut.','Placeat veniam et perspiciatis molestiae earum esse molestiae. Provident magnam quia dolore nisi tenetur mollitia. Ea minima rerum dolorum qui nostrum. Est iure eum iste nesciunt.','1982-12-23'),(14,14,'Repudiandae iusto rerum ut ut.','Eius est et quis veritatis. Blanditiis repellat accusantium eum sit. Est porro et est culpa voluptate et. Rem ut ut repudiandae consequatur quia voluptatem.','Aliquid labore amet dolore omnis in. Qui quibusdam necessitatibus totam sit hic doloremque. Est et cum inventore natus aut.','1989-09-25'),(15,15,'Similique est cupiditate earum molestias quisquam.','Necessitatibus soluta dolor nobis vero enim architecto porro. Quo quidem est consectetur quos eos placeat sunt consequuntur. In aperiam assumenda quo corporis ipsam accusantium. Tempora reprehenderit eos autem quaerat dolores.','Velit nostrum qui consequatur harum. Aut quia voluptas cum quia sit. Mollitia molestias soluta ut quasi dignissimos. Maiores ea qui porro numquam maxime. Atque doloremque quod sint.','1987-09-13'),(16,16,'Laudantium at sed eos quidem voluptatem molestiae recusandae.','Praesentium dolore eaque qui facere rem voluptatem. Quasi aspernatur ipsum aliquid omnis fuga exercitationem. Ullam ut et saepe est laudantium quia.','Amet laborum quaerat voluptatem repellendus consequatur. Nostrum itaque aut ut quo facere. Commodi incidunt voluptates harum alias sit deleniti.','1977-06-24'),(17,17,'Est et maiores et quis dolorem praesentium iusto et.','Quia sed temporibus cum est. Corrupti ut aspernatur qui quis et iure. Excepturi et iusto vitae ea. Recusandae veritatis perspiciatis quam sed quia.','Quas deserunt ut beatae delectus aut error quas velit. Distinctio cum qui voluptatem ducimus. Laborum repudiandae voluptatem aliquam aut numquam voluptatibus. Natus voluptas et nobis vitae id fuga.','1995-11-04'),(18,18,'Exercitationem eius earum pariatur quo incidunt et est.','Occaecati eaque quas dolores sunt. Dolore architecto officiis modi. Quia perferendis asperiores quidem consectetur id. Aut rerum ea molestiae.','Omnis doloremque ut consequatur corporis eos provident deserunt. Deserunt dicta nisi vel blanditiis ut quis et. Tempore repellat ipsum dolorum corrupti et rerum.','1979-02-05'),(19,19,'Et voluptatum voluptatem cumque.','Provident officiis sunt ab sequi voluptatem officia. Repellendus sit laudantium eum ut. Non aperiam distinctio occaecati.','Molestiae incidunt soluta ea et amet culpa magni. Ut magni ea ab dolorem nemo. Et fugiat voluptatem minus quis sapiente modi aliquid. Et ullam suscipit sint beatae nemo ut.','1990-08-01'),(20,20,'Non exercitationem perspiciatis excepturi nihil perferendis.','Quo corporis dolor eum eos repudiandae repellat ut fuga. Id ad assumenda perspiciatis recusandae aperiam et. Quas est sint ut corporis molestias rerum.','Cum et accusamus nulla. Similique omnis ipsum amet aut. Odit non provident modi. Impedit eveniet consectetur sequi.','1988-03-25'),(21,21,'Illo ducimus error non id libero dolorem voluptatum.','Est sint omnis deleniti in eum. Facilis animi porro totam quia debitis tenetur fugiat at. Non qui et nisi molestiae itaque neque.','Delectus sunt tempore impedit nostrum ut. Est sed maiores quos quibusdam sunt et provident. Iure dolorem fugiat ut doloribus. Repellendus velit enim et ex accusamus harum.','1987-05-16'),(22,22,'Occaecati ut odio nesciunt non natus aspernatur non.','Ut dolor asperiores ducimus culpa placeat eos. Distinctio natus voluptatem provident perspiciatis. Sint natus magni molestiae rem veritatis odit possimus est.','Enim perspiciatis enim nihil ut quia esse. Dignissimos numquam veritatis voluptatum beatae.','2000-04-12'),(23,23,'Sit temporibus cupiditate officiis quo et.','Ut maxime sunt quia placeat. Dicta deserunt exercitationem non magnam. Sed cumque et ut.','Fugiat praesentium maxime eligendi ut quo. Maxime facilis a eius recusandae in ut. Quo et ullam dolor commodi unde recusandae.','1983-04-08'),(24,24,'Animi facere quod ipsam nostrum voluptate.','Aut illo occaecati est sit tempore et. Perspiciatis molestias dolor et est qui eaque et ipsam. Aut illo accusamus voluptas quod et. Recusandae consequatur magnam dolor facere voluptatibus. At cumque in cupiditate harum pariatur nihil similique.','Velit est quia commodi pariatur alias esse. Tenetur ea rerum inventore provident debitis omnis. Voluptatem optio beatae vel natus. Deserunt vel est est qui fugit.','1992-09-20'),(25,25,'Sequi et architecto et sed sed inventore velit.','Repellendus consequatur nemo assumenda et dolorem blanditiis debitis totam. Incidunt facilis nesciunt vitae voluptates ipsum quis veritatis. Ducimus sit aut quasi quod.','Quasi impedit aut ullam praesentium quibusdam. Itaque est voluptatem distinctio. Nihil ipsam molestiae impedit error. Reprehenderit quia incidunt animi earum dolores dicta quia.','2011-03-30'),(26,26,'Rerum minus expedita cum explicabo at.','Id voluptatem nihil et repellat et cum sit. Rerum impedit est sunt recusandae et. Laboriosam facilis sint totam officia itaque.','Tenetur non atque tempora incidunt. Qui culpa maxime ut ducimus.\nNon tenetur incidunt labore. Eos vero non aspernatur pariatur. Repellat recusandae aut iste ad laboriosam.','1992-09-24'),(27,27,'Similique fuga ratione adipisci repellat quos esse beatae.','Quibusdam voluptatem delectus ipsa et qui dolore. Ea optio amet perferendis et.','Eaque beatae non quisquam dolor. Et aut velit et error dolores. Explicabo accusantium eius non et corporis. Voluptas porro eos accusamus.','1996-06-26'),(28,28,'Reiciendis quasi facere consequatur adipisci quo sed.','Magni itaque necessitatibus itaque asperiores eligendi. Impedit molestias libero consequatur. Ipsa ipsam et cupiditate sint. Ut ut ipsa eaque nam sit.','Voluptatibus ipsam assumenda vel temporibus omnis fugiat error. Libero voluptatum id quibusdam non quam veniam magni. Quis ipsa error debitis exercitationem qui. A voluptatem sit veritatis quas.','2014-09-15'),(29,29,'Provident consequatur cumque est.','Minima id labore ut voluptatibus. Nihil esse eveniet ullam rem officiis ipsum.','Eveniet animi eaque ut sed mollitia perspiciatis. Aut cupiditate cupiditate et aut. Commodi provident minus ipsum ea aspernatur omnis.','1980-01-29'),(30,30,'Qui alias asperiores dolorem non.','Quidem quos at qui maxime. Non voluptatem ut aut omnis officiis commodi quam. Doloremque fugit quam eligendi neque facilis repudiandae. Maxime dolor qui a blanditiis eaque.','Et eveniet quos quasi laborum error. Aliquam quia numquam ea tempora aut facilis. Ipsa velit a ea molestias voluptatem. Repudiandae ut dolore sit earum.','2004-03-04'),(31,31,'Aut ducimus occaecati est.','Accusantium in quis voluptas minima veritatis explicabo. Et sed aperiam possimus fugit atque molestiae consequatur.','Beatae doloremque cum aspernatur aliquam. Et nihil at fugiat sint nihil et. Dolores aspernatur et nesciunt ab et assumenda tempora. Ipsum aliquam velit ullam.','1979-01-06'),(32,32,'Ipsum nihil et ex.','Asperiores quas deleniti ea aut. Cumque libero omnis ut ut molestiae reiciendis enim. Assumenda reiciendis consequatur molestiae. Deserunt sed aut omnis similique illum.','Dignissimos enim numquam aliquid vitae. Quidem ea dolores consequatur iste sed sit inventore. Laudantium aperiam qui vitae voluptas voluptas et. Perspiciatis beatae sunt explicabo iste.','2012-09-21'),(33,33,'Neque atque illum quae vel.','Eum sit nostrum eos non ut. Quia ut error in in et ratione. Soluta voluptatum dolores sapiente nihil tenetur. Odio excepturi molestiae nihil ratione ratione sunt.','Eaque distinctio ducimus ea atque. Laudantium cum aut corrupti asperiores velit minima.','1996-04-01'),(34,34,'Aspernatur commodi temporibus qui quasi.','Delectus labore veritatis officiis debitis. Eveniet rerum est dolores ea labore. Possimus assumenda voluptas quo qui. Corrupti consequatur aut temporibus quia. Consequatur qui dolores animi repellendus quo optio quisquam.','Aut cupiditate delectus est dolores. Minima odit et quaerat doloribus magni est. Dolore suscipit assumenda nostrum sequi. Pariatur autem ut sit eveniet veritatis quisquam.','2007-07-20'),(35,35,'Voluptatem hic est sit ut.','Quia et quia doloribus. Molestiae eveniet aliquam porro ad voluptatibus magnam. Earum ea dolor explicabo assumenda.','Laboriosam officia officiis nihil quae. Molestiae voluptatem nesciunt sequi et sit.','2010-09-18'),(36,36,'Et necessitatibus hic est fuga porro.','Odio rerum est et omnis ut id. Dolores animi eos magnam necessitatibus quas illum qui. Ut dolore ducimus quam tenetur. Aut voluptas architecto est eum. Et saepe officiis earum enim numquam.','Quaerat recusandae cupiditate numquam porro. Id aut vel pariatur aut. Pariatur omnis nulla voluptas error.','2014-08-06'),(37,37,'Ipsum voluptas iure ducimus aut eum sequi exercitationem.','Illum amet sed qui. Dolore architecto praesentium culpa suscipit iste dolores. Adipisci nisi nesciunt a molestiae ipsam. Reprehenderit error cum qui optio aut.','Id similique eum occaecati. Amet praesentium et qui nobis sint id rerum. Vel eos neque mollitia dolores.','2004-07-23'),(38,38,'Et autem vitae pariatur repudiandae eveniet earum.','At non vel fugit ullam eos harum eaque. Voluptatem aut omnis veniam ullam nobis quae odio. Distinctio qui vel in et sint temporibus vel. Cupiditate voluptas illo corporis error. Ut omnis vel dolores distinctio consequatur veniam voluptas.','Aut porro dicta aspernatur possimus expedita. Quia laborum nemo repudiandae enim nesciunt. Rerum quo molestiae et sint. Quisquam repellendus molestiae autem fugit ex nostrum enim dolores.','2012-10-27'),(39,39,'Ut eaque beatae voluptatibus atque.','Quis dolore dignissimos est saepe odit. Aut iusto quis aut ratione officiis aut non. Eaque at vero eligendi omnis velit. Aut inventore dolor ea iste quaerat.','Dolorem ad molestias commodi minima voluptatum et. Vero asperiores expedita sunt doloremque qui voluptatem vel. Modi at consequatur ad non explicabo modi.','1980-12-13'),(40,40,'Et asperiores culpa blanditiis laboriosam neque aut.','Enim odit quia doloremque aspernatur. Iusto et est voluptates enim ducimus. Sint dolores ad adipisci sint facilis voluptatem deleniti officiis. Hic quaerat adipisci nisi.','Adipisci modi exercitationem quis adipisci animi voluptas. Ipsum molestiae atque ut ut. Quas libero vel dignissimos perspiciatis debitis nobis praesentium. In placeat dolorum voluptas cum.','2015-03-07'),(41,41,'Sapiente tempora expedita sunt quidem qui similique.','Tenetur occaecati et eos nisi voluptatem. Id impedit similique sint maiores qui sequi qui. Mollitia voluptates rerum quia nam enim dolores est. Assumenda totam enim dolores delectus dolor sit rerum.','Autem non soluta dolores neque et et qui. Ab ut molestiae et dolores quis at. Doloremque est illum enim est.','1978-05-30'),(42,42,'Sunt est a suscipit quidem quia.','Pariatur nobis autem corrupti quaerat illum minima. Cumque aut rem sint est pariatur occaecati. Ullam voluptatem quod enim velit quasi.','Laborum autem iste vitae corrupti veniam eum. Aut accusamus qui quibusdam cum sit accusamus ducimus. Rerum sed voluptates voluptatum impedit. Soluta impedit error nemo et cumque.','2008-03-18'),(43,43,'Eum aut modi molestias delectus.','In illum aut sed. Alias consequatur omnis iste praesentium dolorum qui autem. Odit error dolorum iure voluptate rerum quam. Eligendi eos dolores eaque nobis pariatur ea. Ut nam veniam qui error praesentium ab maxime.','Deleniti inventore est velit accusantium voluptatem. Dolor natus earum accusantium alias sed nobis. Aut voluptates atque corrupti debitis vitae. Itaque autem odit aut amet fugiat.','2013-01-22'),(44,44,'Inventore aut eum ab.','Similique corrupti vitae id ducimus veritatis voluptatem. Aliquam commodi iste magni dolorum eligendi atque. Molestiae eum tempora officia in.','Rerum magni laboriosam aut iure ut et laborum. Voluptatum sunt mollitia ipsam voluptatem iusto nam quas. Modi cumque est repellendus amet ut est quia.','1981-12-21'),(45,45,'Eius quo voluptates veniam alias facilis dolorem.','Aspernatur pariatur quisquam officiis sed ab repudiandae occaecati. Necessitatibus ut error consequuntur consectetur minima. Ea occaecati provident molestias rerum similique culpa. Veritatis eligendi aliquam magni occaecati inventore. Itaque consectetur ullam non facere ducimus.','Asperiores aut ipsam dicta doloremque amet eos qui. Quae rerum sit voluptatem eius. Ut ea incidunt officiis sit. Omnis quia est sed sint eum accusantium suscipit et.','1993-03-31'),(46,46,'Laudantium qui sint doloribus et.','Et nihil veritatis aut perspiciatis quibusdam et delectus. Saepe delectus molestiae est quibusdam tempore repellat minus. Non quis doloremque qui excepturi iure repellat. Natus possimus est et eius illum perferendis. Aspernatur hic illum ipsam excepturi laudantium eius.','Possimus sequi sapiente facilis qui cupiditate. Tenetur ea eveniet libero perferendis dicta. Vel qui at sapiente non deleniti consequatur. Accusamus qui earum delectus ex nemo.','1980-01-12'),(47,47,'Dolorem qui impedit vel dignissimos.','Tempora dignissimos sed quo aut totam ut. Eius eos eum pariatur aspernatur ab aspernatur maiores.','Voluptatem qui esse culpa ut nihil pariatur. Voluptas doloremque modi suscipit quasi voluptate natus. Sunt nihil illo officiis perspiciatis. Unde magni quae voluptatum velit aut.','1981-08-17'),(48,48,'Quae sed assumenda illum vero fugit rerum.','Expedita ab rerum est error a. Ea illo exercitationem mollitia enim expedita nobis non.','Consequuntur quam qui corporis cum earum. In commodi vel laboriosam vel. Nisi natus at autem officia est harum. Et neque quo et praesentium.','2003-04-26'),(49,49,'Ad eos veniam facilis voluptas.','Aut odit est placeat distinctio consequatur numquam et. Ratione itaque molestias consequatur assumenda. Maxime quos modi aut vel maiores eius quia.','Officiis nostrum quisquam occaecati dolores sit aspernatur animi. Natus nesciunt reprehenderit voluptatum vel porro eum. Officia dolorem velit consequatur est cum.','2001-03-06'),(50,50,'Voluptate commodi ea quos laudantium porro.','Porro earum et autem explicabo enim quis dolorem. Vero quia aut sequi. Magni necessitatibus doloribus quo libero. Sint eveniet voluptatem ut voluptate id.','Repellendus qui vitae molestias doloremque. Beatae eum amet odit accusamus eveniet aut voluptatem. Qui blanditiis occaecati id et et assumenda. Nisi rerum corrupti corporis mollitia quibusdam.','1974-05-06'),(51,51,'Culpa quo numquam delectus consequatur quis quo.','Numquam est officiis autem vero eos. Nihil nemo rem quis et soluta. Voluptatem qui porro aut ipsum.','Ut fugit reprehenderit non cupiditate mollitia excepturi labore. Quo dolores a quo eum doloremque cupiditate eos. In facere enim officiis voluptates ipsa tempore vel. Ad rem error rerum veniam.','1980-09-10'),(52,52,'Beatae dignissimos mollitia maiores.','Consequatur omnis et harum consequatur voluptatem soluta. Cumque harum rerum recusandae vel sint. Praesentium modi sint cumque doloremque nobis dolore qui.','Quaerat et et sint saepe vel. Ratione veniam vel aut rerum et numquam at et.','1976-01-26'),(53,53,'In nihil officiis accusamus sit ipsa laborum.','Amet ipsum consequatur est enim mollitia provident. Autem veritatis non ipsam laudantium possimus laborum dolores. Neque vel unde aut dignissimos molestiae sequi. Repellat et necessitatibus quod maxime.','Sed ut porro similique dolor. Et distinctio distinctio ducimus. Dolor qui at sit saepe. Sunt ad et quibusdam veniam itaque veniam.','1997-12-09'),(54,54,'Laborum corporis rem quis ut autem.','Quia rem cum voluptate voluptate sit minima. Fugit tempore non ut atque ad illo incidunt. Tenetur veritatis natus rerum vel et expedita laudantium. Repudiandae repellat facilis reprehenderit natus.','Nihil et ipsum natus officia. Eum et molestias eos fugit dolor ut. Dolorem quam magnam ex consequatur. Harum reiciendis voluptatem magni voluptas omnis.','1989-06-24'),(55,55,'Doloribus sed sed voluptas voluptate.','Voluptate incidunt ratione corporis. Aliquam facere cum dolores eos sint totam non aut. Vel ut ipsum eos dolores explicabo praesentium quaerat atque. Laboriosam nemo autem et minima dolorem.','Recusandae pariatur doloremque deserunt ut saepe architecto. Consequatur excepturi eum voluptatem veritatis. Dolor quisquam at qui facilis minima ut minima.','1993-02-12'),(56,56,'Accusamus omnis quos optio officia et.','Quasi labore delectus corporis et nesciunt quam. Voluptate dolorum totam voluptatem delectus fugiat. Sequi quaerat dolores ipsa. Similique blanditiis et neque omnis doloremque dicta voluptatum.','Doloribus veniam soluta quae placeat eum fugit. Possimus tenetur voluptatem eum. Ut provident dolor ad sint totam. Ipsam molestiae commodi sed animi eum culpa fugit doloribus.','1989-08-25'),(57,57,'Et sunt nulla sit odit qui quam animi accusamus.','Qui explicabo vel maxime recusandae exercitationem ipsum. Omnis minima est tenetur dicta cupiditate eligendi. Exercitationem distinctio minus ad dicta illo.','Sunt sapiente aperiam sed adipisci dolore. Aut recusandae et autem aperiam illo. Et vel quae et ipsa et impedit magni. Expedita laudantium et aut adipisci ut alias eos.','2012-12-09'),(58,58,'Rerum ea commodi quod commodi ipsum nihil tempora.','Nisi sint dolorem quia excepturi architecto eum. Suscipit autem qui omnis asperiores. Nihil totam deleniti architecto facilis vero sit non. Est quod cupiditate rem voluptatibus.','A et architecto mollitia ut. Voluptates consequatur et quo placeat magni voluptas laborum. Et laudantium vel esse quasi ut. Maiores iure accusantium quia. Quos doloribus iure ea incidunt.','2010-01-10'),(59,59,'Illo consequatur deserunt debitis illo.','Doloremque deleniti quod qui est harum. Sit blanditiis voluptatem est et quae reiciendis incidunt. Iure laboriosam veritatis mollitia et tempora ducimus.','Quibusdam esse dolorem quod optio provident sequi accusantium. Quas consequuntur eos mollitia ducimus quidem eius deleniti.','1976-06-02'),(60,60,'Et voluptatibus distinctio ut quia sunt dolore.','Occaecati voluptas eaque non voluptatem molestias maiores sint id. Fugit deleniti ut enim amet unde omnis. Velit laborum et nulla minus numquam.','Repudiandae beatae quos commodi atque aliquam et odit. Accusantium ut totam quaerat. Nihil blanditiis aut harum nostrum impedit facere.','2003-07-17'),(61,61,'Voluptatem quos sed sit quod enim delectus quo.','Accusamus eaque quasi impedit pariatur eligendi. Tempora molestiae cum veniam et. Ad officia corporis eveniet quo. Expedita accusantium corrupti assumenda sit.','Pariatur quis totam quia adipisci. Possimus laborum nihil ut. Ut fugit quia doloribus aliquid.','2017-06-29'),(62,62,'Quo unde atque ratione.','Amet libero expedita necessitatibus. Fuga non fuga deserunt quisquam quas. Dolorem perferendis doloribus necessitatibus debitis cupiditate eaque officia. Harum eius in magnam et.','Voluptas et quidem non sit amet nihil quia. Non et blanditiis quidem fugit eum. Dolores qui quasi saepe commodi est quis. Sed eum distinctio et.','2008-09-23'),(63,63,'Quaerat quo qui provident perferendis.','Quo qui reiciendis pariatur vero repellendus voluptatem adipisci. Quae quod sed quo fuga fugit exercitationem accusamus autem. Rerum animi sit ad ipsum aut. Ex quidem corporis dolore id.','In doloribus possimus est sint. Aut velit nam non nemo. Maxime quia tenetur inventore quia non. Sint et nihil sunt.','1992-06-01'),(64,64,'Dolor laboriosam ut aperiam et sed fugit tempora.','Amet nesciunt corporis consequatur quam quae dolor perspiciatis enim. Ut aliquid et voluptatibus quia atque. Est dolor molestiae est sed molestias aspernatur quas.','Rerum vero excepturi dolor fugit. Similique expedita labore eius dolores accusamus. Labore et rerum voluptatibus temporibus aut animi quaerat.','2013-07-01'),(65,65,'Placeat qui omnis assumenda.','Explicabo ipsum autem nihil maxime. Quod molestiae quis fugit dolores velit. Officia ea cupiditate dolores voluptatibus et. Et quam adipisci laboriosam aliquid.','Voluptate deleniti tempore et similique quod dolorum laborum. Deleniti nam maxime consequatur et. Maxime ut animi ducimus adipisci cumque mollitia.','1978-03-13'),(66,66,'Non explicabo voluptatum blanditiis ut.','Iste est nisi aut aut consequatur quam porro enim. Quaerat sequi doloribus aut eos minus cumque. Laudantium in sequi mollitia ipsam veritatis accusamus incidunt est. Quas neque minima eum qui culpa nesciunt adipisci.','Recusandae cupiditate minima dolorem reprehenderit. Velit harum quo autem est qui dolorem deserunt. Dignissimos nam dolores id sed ducimus quae ex. Rem minus aut recusandae facere ut illo nulla.','1974-07-24'),(67,67,'Provident ex nihil sequi aut iste omnis.','Pariatur veritatis aliquid qui dignissimos molestiae praesentium. Deleniti eos nam et sit voluptatibus consectetur sit. Soluta facere provident sed quas non neque et. Dolorum ut eos veniam amet voluptas. Fugiat quia sint expedita ea.','Eligendi quidem nesciunt ipsa. Doloremque perferendis esse sapiente et minima suscipit at. Eos laborum ut adipisci maxime rerum suscipit. Minus nemo voluptatem saepe sit culpa fugiat.','1971-06-22'),(68,68,'Et at maxime quos eaque sequi dolorem aspernatur.','Veniam voluptatem minima earum minima et culpa. Eaque repellat similique sunt voluptatum et. Sit illo ut est sint. Dolor omnis aut amet numquam qui.','Non minima ut autem est eaque. Est in velit blanditiis corporis a. Enim fugiat nam dolorem nihil asperiores. Nisi fugit quasi nulla ea placeat qui et ducimus.','1979-07-25'),(69,69,'Consequatur et repudiandae qui ipsum illo voluptatem dolorem.','Optio incidunt blanditiis recusandae iste eum odio vel. Eum cumque vel debitis aliquam. Quia eius illum enim sequi unde veniam. Quia enim exercitationem placeat.','Incidunt sapiente voluptates exercitationem sit enim voluptatem saepe. Corrupti culpa fugit qui. Eveniet assumenda culpa iure. Sed facere qui eos repellat officia quo eos.','1993-04-29'),(70,70,'Dolores enim earum deserunt molestiae nemo placeat.','Quia voluptatem quaerat vel. Et est consequatur et perferendis tempore. Numquam eos et tempora mollitia. Itaque perspiciatis est porro.','Aut est minima nemo beatae excepturi nihil et. Voluptas tenetur ad veritatis nihil dicta est. Et eos ut ad ex ea rerum. Id necessitatibus adipisci dolor beatae similique.','1990-10-06'),(71,71,'Voluptatem tempora numquam ratione iusto laborum excepturi illum.','Fugit ducimus at accusamus est perspiciatis deserunt reiciendis. Optio et voluptatem quas pariatur. Est autem repellat cumque nisi labore illo nostrum nihil.','Ducimus unde repellat necessitatibus dignissimos rerum. Placeat minus et commodi aliquam delectus repellendus veniam. Qui nam quis et consequatur voluptatem iste.','1997-03-03'),(72,72,'Consequatur laudantium quia voluptatibus.','Maiores sapiente nemo voluptatibus amet molestiae. Animi exercitationem quia rerum vel consequatur nemo vel. Tenetur veniam facere illum ea.','Quisquam aliquid cumque nesciunt corporis soluta. Assumenda omnis ipsa officia optio. Fuga quia voluptas qui et vitae quod velit mollitia.','1989-08-16'),(73,73,'Exercitationem laborum quia dignissimos ea necessitatibus.','Qui qui in sed quis. Quo officiis earum sit similique ut doloribus.','Fugit et vitae non voluptatem quo modi voluptatibus. Sunt ipsum voluptas magnam cumque deserunt non. Beatae optio voluptatem perspiciatis est.','2005-11-14'),(74,74,'Occaecati cupiditate ipsam aut ipsam commodi.','Et recusandae deserunt consequuntur aut quia labore nostrum. Sunt eligendi aut modi sunt odit. Et quibusdam placeat distinctio facilis.','Qui molestiae amet officia quae. Et voluptas quidem ut nihil. Officiis aut ut aliquam non.\nAt hic ducimus dolor autem porro. Doloremque consequatur beatae quo et.','2001-10-06'),(75,75,'Et ea provident explicabo unde qui consequatur.','Non totam cum ratione consequatur assumenda culpa. Vitae quis neque neque voluptatem adipisci.','Itaque enim et placeat cupiditate et perferendis. Et enim quis hic est sit natus tempora.\nRerum iusto assumenda et non nihil commodi sunt. Aut deleniti id commodi et et.','2009-09-29'),(76,76,'Numquam rerum laborum et illum odio iste harum.','Esse quaerat magnam alias recusandae molestias suscipit voluptatibus. Eos aspernatur magnam maiores sed perferendis quod autem. Et quia aut facilis qui voluptatem.','Molestias laborum eum asperiores adipisci. Ut eum est et vel nobis eum. Quibusdam facere culpa magni earum.','1998-06-30'),(77,77,'Omnis expedita in quis.','Voluptatem doloribus quaerat voluptas et. Quos quo voluptas cupiditate soluta. Dolores architecto itaque minus maxime id ipsum. Ut reiciendis ex illo voluptas laudantium.','Incidunt corporis quia laudantium ut sed quam repellat. Quae ut molestias amet magni. Aut dicta odit quam sapiente eligendi.','2005-08-15'),(78,78,'Quia et occaecati doloremque tempore.','Quam sapiente alias sint sapiente. Quod dolores tenetur illo.','Est vitae vel rem aperiam deserunt. Dolor qui ut odit est sit dicta est. Totam pariatur quam nulla aliquam minima dolore.','1984-10-10'),(79,79,'Nobis suscipit quia aut eos.','Ipsam nam qui qui iure repudiandae. Beatae dolorem quod non voluptatum. At illum adipisci minus aliquid distinctio enim. Possimus debitis quis ullam quas in quos voluptas.','Ut id vero atque qui molestias est id. Qui aliquid perferendis aut ad sint nihil commodi. Et et impedit sint sit quae ipsa harum tempora. Numquam ut quod distinctio sint consequatur.','2019-08-12'),(80,80,'Sed aut quibusdam voluptatum rerum.','Qui repellendus dignissimos aliquam nostrum qui at explicabo. Ducimus voluptatem provident est ullam eius.','Ab dolorem quasi vel earum et quis eum placeat. Quod iure ut molestias sed. Sint voluptatibus odio est maxime sit sed quo illo. Voluptas ea sit repudiandae quo.','2004-09-18'),(81,81,'Est accusantium ut nesciunt quia quam delectus.','Dolorem distinctio enim cupiditate temporibus dolor esse est. Laboriosam qui nisi consequatur adipisci voluptatem aut consequatur. Adipisci expedita doloremque aut ipsam veniam illum earum.','Sit fugit incidunt ex nihil. Repellat voluptatum itaque ea et nam perferendis officiis placeat. Ad aut assumenda velit vero perspiciatis et molestiae.','1985-03-04'),(82,82,'Amet cumque atque assumenda rerum.','Eos voluptatem ratione voluptatum quis iure fuga expedita et. Sed velit ut ut.','Aut et eius minima. Repellat illo placeat reprehenderit sequi rem voluptas repudiandae. Ut nam facilis excepturi sunt perferendis architecto aut exercitationem.','2019-06-10'),(83,83,'Delectus nobis ducimus praesentium repudiandae suscipit.','Itaque fugit voluptatem fugit veniam possimus. Enim odit rerum et nemo sed dolore ea. Cum est dignissimos et id excepturi.','Nobis corrupti eum neque architecto. Ullam eligendi sint voluptates molestias voluptas. Ut est dolor odit omnis. Et eius nam libero eveniet aperiam molestiae sed.','2007-01-12'),(84,84,'Vel veniam aliquid neque dolorum laborum delectus qui.','Illum velit excepturi ex aut ut cupiditate aliquam. Laudantium velit maiores ab incidunt repudiandae.','Aut ut velit non id voluptatem. Eligendi accusantium et ad aliquid fugiat et deleniti. Ipsum qui eos repellat blanditiis harum dolore magnam.','1989-03-31'),(85,85,'Laboriosam qui sapiente qui necessitatibus est est.','Unde hic praesentium impedit et provident. Assumenda quas quo saepe. Eligendi in itaque dolorem voluptatem exercitationem enim ipsa nesciunt. Ipsam aspernatur reiciendis commodi.','Voluptatem commodi optio quis nihil qui iusto vero dolorem. Eum sit voluptatem doloribus et dolor nulla occaecati. Ut autem porro eum est eaque.','1986-07-11'),(86,86,'Sit optio voluptas vitae illum provident qui.','Nulla commodi incidunt quis quo. Quisquam aut reiciendis tempora expedita quia quis voluptate. Architecto quis nesciunt facilis ullam provident.','Totam fuga excepturi accusantium officiis totam fugiat. Quam error incidunt incidunt excepturi. At quia ut asperiores quam. Voluptas rem quos unde expedita et repellat sed.','1985-06-02'),(87,87,'Quia vitae esse sit aliquam et.','Modi amet in autem asperiores nostrum et ullam. Et consequatur similique sint accusamus delectus sunt. Et sit facere vel dolorum autem unde. Odio non vel ut voluptate.','Consequuntur aut pariatur molestiae omnis et temporibus eveniet. Aut quisquam consequatur ipsam reprehenderit. Et velit nihil eos sit blanditiis.','1994-02-25'),(88,88,'Provident reprehenderit impedit voluptatibus veniam.','Ea vitae distinctio numquam ea omnis aut ut praesentium. Voluptate animi ipsa ut soluta voluptatibus dolore iste sunt. Deserunt qui dolore qui a cum aut optio.','Cumque suscipit sunt dolorem rerum impedit qui. Quis molestiae debitis corrupti aliquam et. Et quia ratione qui culpa nesciunt accusamus. Eligendi vero et iusto est in.','1986-03-18'),(89,89,'Voluptatibus et non nobis.','Quia vitae odit et officia qui. Quaerat non est nulla incidunt. Maiores nisi consequatur accusantium est.','Id aut consectetur illum excepturi. Esse accusantium exercitationem quasi exercitationem sapiente ut quidem id. Earum voluptate nam culpa.','1989-02-16'),(90,90,'Rerum dignissimos quis qui molestias id numquam.','Ut eum qui tempora nostrum eos doloremque. Delectus enim rem consequatur officiis impedit. Nemo et aut qui non temporibus molestiae cum. Earum itaque quia voluptate quo.','Aut voluptatem fuga est et non voluptatem. Consequatur vitae delectus suscipit qui non consequatur tempore velit. Consequuntur cupiditate maiores adipisci qui quam voluptatum hic.','1975-03-09'),(91,91,'Cumque sunt excepturi nobis nihil consequatur ut tenetur saepe.','Quo non delectus odio. Et repellat ipsum suscipit qui ut quidem. Ipsum dolor pariatur similique consequatur.','Quisquam nemo quis qui sit. Officiis occaecati adipisci dolore nisi deserunt voluptatem ut. Ad accusantium qui velit iste facilis.','1973-02-27'),(92,92,'Esse quis asperiores nostrum sequi distinctio deserunt.','Minus voluptatum est qui id at porro. Sint ea enim ab facere. Dolorum sint voluptas numquam minus perspiciatis dicta est. Error voluptas sit id quas.','Velit laboriosam rerum sequi et quasi. Dolorem sapiente et ipsum sed. Impedit dicta est amet impedit est accusamus.','1982-07-27'),(93,93,'Est quibusdam deserunt velit et et.','Eos voluptates est voluptas. Voluptas cupiditate labore nesciunt mollitia nostrum vero sequi. Nesciunt hic assumenda blanditiis. Molestias ullam quibusdam eum et laudantium.','Omnis natus vitae nemo assumenda porro. Dolore consectetur labore dolor est molestiae voluptatem fugiat. Eum commodi accusantium perspiciatis blanditiis. Minus distinctio qui est ea quibusdam.','1986-04-24'),(94,94,'Itaque autem assumenda ex est fugit.','Libero id harum dignissimos quia nam et neque sint. Ipsum accusamus qui accusamus vero pariatur dolores rerum. Qui et et sit similique illo. Delectus est ad aliquam a fugit ducimus nihil.','Dolores id ipsa vel aut perspiciatis sapiente veniam. Culpa doloribus ea doloremque ipsum dolorem culpa. Et fugit quidem qui sunt. Nihil est aliquid doloremque corporis alias aut debitis sit.','1977-01-16'),(95,95,'Rerum dolorum eum omnis modi nihil.','Consectetur id omnis amet. Dolorem voluptate nesciunt rerum et velit. Repudiandae voluptatem ut deserunt laborum. Laudantium debitis laboriosam earum cupiditate. Qui amet voluptate animi et facilis et.','Nihil sapiente assumenda nisi aliquam adipisci sequi aliquid. Aut quis voluptas asperiores maiores a. Aliquam veritatis veniam sequi quibusdam et. Vel dolor ullam dolorem porro cum.','2015-02-12'),(96,96,'Odio ipsa placeat aut ipsam magni aut aperiam.','A sequi ut vel cum eaque eum sint eos. Itaque facere repudiandae optio quae. Magni molestiae et iure magnam expedita. Odio alias quam molestiae blanditiis nostrum ut deleniti.','Sequi natus quis quia aut distinctio. Dicta numquam quia architecto modi. Saepe velit corrupti nobis est hic. Omnis suscipit qui ut culpa velit repudiandae eum repudiandae.','1980-08-14'),(97,97,'Ab ipsum corporis nulla et explicabo aut expedita.','Dignissimos dolor eveniet iure sed dolor illum. Aspernatur suscipit accusamus a consectetur voluptatem quo. Quibusdam voluptas voluptas eum.','Reiciendis voluptas totam aut aliquam repellat. Praesentium esse et minus molestiae.','1976-02-03'),(98,98,'Vero quis necessitatibus sit porro quaerat delectus.','Molestiae sequi non vitae qui reprehenderit. Quam sequi at aspernatur aperiam pariatur porro ullam. Est qui unde doloribus totam nemo dignissimos reiciendis.','Ipsa repudiandae fuga excepturi debitis odio. Et id et saepe dolores et sed sunt qui. Et impedit voluptate expedita sed tempora ab perspiciatis.','1972-06-11'),(99,99,'Ut alias sint repudiandae nostrum vel voluptatem vel.','Impedit similique quidem dolorem in adipisci amet assumenda. Perspiciatis explicabo error voluptatem iure dolore.','Dolore error esse architecto animi. Alias possimus blanditiis dolorum reprehenderit voluptatem omnis. Sunt dicta vitae provident dignissimos dolor laborum vel.','2016-03-15'),(100,100,'Voluptas labore error deleniti ipsa commodi deleniti.','Quis ut aliquid repellendus impedit nobis et. Molestias perferendis quibusdam sequi fugit aspernatur dolor nam. Quis nihil enim sint sit.','Provident rem sit voluptatem quia deleniti occaecati. A qui tenetur temporibus ad qui vitae natus. Necessitatibus voluptatem officia quia aut nihil quisquam unde. Nisi enim quas eaque est rerum.','1994-01-15');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'myblog'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-21 10:42:37
