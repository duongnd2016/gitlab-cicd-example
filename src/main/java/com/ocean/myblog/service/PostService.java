package com.ocean.myblog.service;

import com.ocean.myblog.service.dto.PostDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ocean.myblog.domain.Post}.
 */
public interface PostService {

    /**
     * Save a post.
     *
     * @param postDTO the entity to save.
     * @return the persisted entity.
     */
    PostDTO save(PostDTO postDTO);

    /**
     * Get all the posts.
     *
     * @return the list of entities.
     */
    List<PostDTO> findAll();

    /**
     * Get the "id" post.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PostDTO> findOne(Long id);

    /**
     * Delete the "id" post.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
